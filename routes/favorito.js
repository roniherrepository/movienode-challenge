'use strict'

var FavoritoController = require('../controllers/favorito');
let FavoritoMiddleware = require('../middlewares/favoritos');
const Router = require('koa-router');
const api = new Router();

api.get('/movie/:Title', FavoritoMiddleware.favoritoValidator, FavoritoController.getFavorito);
api.get('/movies', FavoritoMiddleware.favoritosValidator, FavoritoController.getFavoritos);
api.post('/movie', FavoritoMiddleware.searchAndReplaceValidator, FavoritoController.searchAndReplace);
api.put('/movie/:Id', FavoritoController.updateFavorito);
api.delete('/movie/:Id', FavoritoController.deleteFavorito);

module.exports =  api;
