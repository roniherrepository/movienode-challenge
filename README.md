# Movienode-challenge

#### Desarrolado para ser ejecutado por defecto para Docker pero tambien puede ser utilizado en localhost.

## Instalación


### Docker
Ejecutar los siguientes comandos en la raíz del repositorio.

`docker-compose build`

 `docker-compose up`


### Local
Para ejecutar en local se debe cambiar la ruta del mongoDB en el archivo `index.js`.

Por defecto traera la ruta `mongodb://mongo/cursosfavoritos` que se utiliza para el docker, debemos cambiarla por la ruta `mongodb://localhost:27017/cursosfavoritos` para la ejecucion en local.


Ahora podremos ejecutar con los siguientes comando para iniciar la ejecución local.

`npm install`

`npm start`

#### Consultas Postman

En la raíz del repositorio encontramos la carpeta `postman` donde se encuentra la colección de consultas de Postman tanto para Docker como Local, para facilitar la prueba de la API.

###### Dev Ing. Roniher J. Vilchez J.