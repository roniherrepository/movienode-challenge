'use strict'

const fetch = require("node-fetch");
const apiKey = "53279e81"
const url = "http://www.omdbapi.com/"
var Favorito = require('../models/favorito');

async function getFavorito(ctx){
    try{
        const title = ctx.params.Title.split(' ').join('+');
        const pelicula = await movieApi(title,ctx.request.headers.year);
        if(pelicula.Response === 'False'){
            ctx.body=pelicula.Error;
        }else{
            let busqueda = await Favorito.find({Title: pelicula.Title});
            if(busqueda.length===0){
                const favorito = newFavorito(pelicula);
                ctx.body = await favorito.save();
            }else{
                ctx.body = busqueda;
            }
        }
    }catch(err){
        ctx.body = {message: 'No se encontro la pelicula'};
        ctx.status = 404;
    }
};

async function getFavoritos(ctx){
    try{
        const page = ctx.request.headers.page || 1;
        const options = {
            page: page,
            sort: { Title: 1 },
            limit: 5,
        };
        ctx.body = await Favorito.paginate({},options);
        if(ctx.body.length===0){
            ctx.body = {message: 'No se encontraron peliculas'};
            ctx.status = 404;
        }
    }catch(err){
        ctx.body = {message: 'Error al buscar las pelicula'};
        ctx.status = 500;
    }
};

async function searchAndReplace(ctx){
    let pelicula = await Favorito.findOne({Title:ctx.request.body.movie});
    try{
        if(!pelicula){
            ctx.body = {message: 'No se encontro la pelicula'};
            ctx.status = 404;
        }else{
            pelicula.Plot = pelicula.Plot.split(ctx.request.body.find).join(ctx.request.body.replace);
            await pelicula.save();
            ctx.body = pelicula.Plot;
        }
    }catch(err){
        ctx.body = {message: 'Error al buscar y reemplazar'};
        ctx.status = 500;
    }
}

async function updateFavorito(ctx){
    var update = ctx.request.body;
    try{
        await Favorito.findOneAndUpdate({'_id':ctx.params.Id},update);
        ctx.body = await Favorito.find({'_id':ctx.params.Id});
    }catch{
        ctx.body = {message: 'Error al modificar la pelicula'};
        ctx.status = 500;
    }
}

async function deleteFavorito(ctx){
    try{
        await  Favorito.findOneAndRemove({'_id':ctx.params.Id});
        ctx.body = {message: 'Se ha eliminado la pelicula'};
    }catch(err){
        ctx.body = {message: 'No se ha eliminado la pelicula'};
        ctx.status = 500;
    }
}

function newFavorito(pelicula){
    var favorito = new Favorito();
    var params = pelicula;

    favorito.Title = params.Title;
    favorito.Year = params.Year.toString();
    favorito.Released = params.Released;
    favorito.Genre = params.Genre;
    favorito.Director = params.Director;
    favorito.Actors = params.Actors;
    favorito.Plot = params.Plot;
    favorito.Ratings = params.imdbRating;

    return favorito;
}

function movieApi(title, year){
    let fetchUrl = `${url}?t=${title}&apikey=${apiKey}`;
    if(year && year !== ""){
        fetchUrl += `&y=${year}`;
    }
    return fetch(fetchUrl)
    .then((respuesta)=>{
        return respuesta.json()
    })
}

module.exports = {
    getFavorito,
    getFavoritos,
    searchAndReplace,
    updateFavorito,
    deleteFavorito
}
