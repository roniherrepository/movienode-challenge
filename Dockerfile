# Node Version
FROM node:14
# Create app directory
RUN mkdir -p /usr/src/app
# Move to directory
WORKDIR /usr/src/app
# Copy the files into workdir
COPY package.json ./
# Install all the dependencies
RUN npm install
# Bundle app source
COPY . .
# Expose the port to get access
EXPOSE 3678
# Run the node command
CMD [ "node", "index.js" ]
