'use strict'

const SARValidator = require('../validator/searchAndReplace');
const FavValidator = require('../validator/favorito');
const FavsValidator = require('../validator/favoritos');

async function searchAndReplaceValidator(ctx, next){
    let validation = SARValidator.schema.validate(ctx.request.body);
    console.log(validation);
    if(!validation.error){
        await next();
    }else{
        console.log(validation.error);
        ctx.body="NO CUMPLE CON EL CONTRATO";
        ctx.status=400;
    }
}

async function favoritoValidator(ctx, next){
    let headerValidation = FavValidator.headerSchema.validate(ctx.request.headers,{allowUnknown:true});
    let paramsValidation = FavValidator.paramsSchema.validate(ctx.request.params);
    if(!headerValidation.error && !paramsValidation.error){
        await next();
    }else{
        ctx.body="NO CUMPLE CON EL CONTRATO";
        ctx.status=400;
    }
}

async function favoritosValidator(ctx, next){
    let headerValidation = FavsValidator.headerSchema.validate(ctx.request.headers,{allowUnknown:true});
    if(!headerValidation.error){
        await next();
    }else{
        ctx.body="NO CUMPLE CON EL CONTRATO";
        ctx.status=400;
    }
}

module.exports = {
    searchAndReplaceValidator,
    favoritoValidator,
    favoritosValidator
}
