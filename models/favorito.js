'use strict'

const mongoosePaginate = require('mongoose-paginate-v2');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FavoritoSchema = Schema({
    Title: String,
    Year: String,
    Released: String,
    Genre:  String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: String
});

FavoritoSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Favorito', FavoritoSchema);
