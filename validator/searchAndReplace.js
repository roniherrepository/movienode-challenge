const Joi = require('joi');

const schema = Joi.object({
    movie: Joi.string()
    .min(2)
    .max(50)
    .required(),

    find: Joi.string()
    .min(1)
    .max(50)
    .required(),

    replace: Joi.string()
    .min(1)
    .max(50)
    .required(),
})

module.exports={
    schema
}
