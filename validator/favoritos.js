const Joi = require('joi');

const headerSchema = Joi.object({
    page: Joi.number()
    .integer()
    .min(1)
})

module.exports={
    headerSchema
}
