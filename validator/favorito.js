const Joi = require('joi');

const headerSchema = Joi.object({
    year: Joi.number()
    .integer()
    .min(1850)
    .max(parseInt(new Date().getFullYear())),
})

const paramsSchema = Joi.object({
    Title: Joi.string()
    .min(1)
    .required(),
})

module.exports={
    headerSchema,
    paramsSchema
}
