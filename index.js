'use strict'

var mongoose = require('mongoose');
var app =require('./app');
var port = process.env.PORT || 3678;
// RUTA PARA LOCAL:  'mongodb://localhost:27017/cursosfavoritos'
// RUTA PARA DOCKER: 'mongodb://mongo/cursosfavoritos'
mongoose.connect('mongodb://mongo/cursosfavoritos',(err, res) => {
    if(err){
        throw err;
    }else{
        console.log('Conexion a MongoDB correcta');
        app.listen(port, function(){
            console.log("API REST PELICULAS funcionando en http://localhost:"+port);
        })
    }
})
