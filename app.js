'use strict'

const Koa = require('koa');
var bodyParser = require('koa-bodyparser');
var app = new Koa();
var api = require('./routes/favorito');

app.use(bodyParser());
app.use(api.routes());

module.exports =app;
